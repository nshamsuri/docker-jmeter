# Docker JMeter

## Performance Test Setup
1 x AWS EC2 instance (master & slaves)

## To install Docker on AWS EC2 instance

1. Connect to your instance via SSH
2. Update the installed packages and package cache on your instance.

    ```
    sudo yum update -y
    ```

3. Install the most recent Docker Community Edition package.

    ```
    sudo amazon-linux-extras install docker
    ```

4. Start the Docker service.

    ```
    sudo service docker start
    ```

5. Add the ec2-user to the docker group so you can execute Docker commands without using sudo.

    ```
    sudo usermod -a -G docker ec2-user
    ```

## To run JMeter master & slaves
6. Run JMeter master.

    ```
    sudo docker run -dit --name master nshamsuri/jmmaster /bin/bash
    ```

7. Run JMeter slaves (Up to n number of slaves). Below examples use to spawn 2 jmmester slaves.

    ```
    sudo docker run -dit --name slave01 nshamsuri/jmslaves /bin/bash && sudo docker run -dit --name slave02 nshamsuri/jmslaves /bin/bash
    ```

8. To inspect network IP for JMeter instances.

    ```
    docker inspect --format '{{ .Name }} => {{ .NetworkSettings.IPAddress }}' $(docker ps -a -q)
    ```

## To start JMeter run script

9. Move your JMeter script into Master instance.

    ```
    docker cp <source-path-to-file.jmx> CONTAINER:/<destination-path-to-file.jmx>
    ```

10. Run the command below to start the test.

    ```
    jmeter -Jjmeter.save.saveservice.timestamp_format="yyyy/MM/dd HH:mm:ss" -Dserver.rmi.ssl.disable=true --n -t /path/to/your/script.jmx -R<Your.Slaves.IPs> -l <output-file.csv>
    ```

11. Restart all docker containers
    ```docker restart $(docker ps -q)```

### Stop & delete all containers
```
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```
### Build docker
```docker build -t nshamsuri/jmmaster .```
### Run docker
```docker run -it nshamsuri/jmmaster```
### Push image to Docker Hub
```docker push nshamsuri/jmmaster```