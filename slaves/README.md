# JMeter Docker - Slaves
This is slave docker image for JMeter

## Prerequisites
- [JMeter base](https://hub.docker.com/r/nshamsuri/jmbase)

## Usage
1. Run JMeter slaves (Change --name value to create multiple slaves)
```sudo docker run -dit --name slave01 nshamsuri/jmslaves /bin/bash```