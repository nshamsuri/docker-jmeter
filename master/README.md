# JMeter Docker - Master
This is master docker image for JMeter

## Prerequisites
- [JMeter base](https://hub.docker.com/r/nshamsuri/jmbase)

## Usage
1. Run JMeter master
```sudo docker run -dit --name master nshamsuri/jmmaster /bin/bash```