# JMeter Docker - Base
This is base docker image for JMeter

## Docker Stack

1. Java 8 slim JRE
2. JMeter 5.1.1

## Plugins

1. [Flexible File Write](https://jmeter-plugins.org/wiki/FlexibleFileWriter/)
2. [Concurrency Thread Group](https://jmeter-plugins.org/wiki/ConcurrencyThreadGroup/)

## Usage

- This image is used for jmmaster & jmslaves